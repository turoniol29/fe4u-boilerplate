export const map = L.map('map');
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '© OpenStreetMap'
}).addTo(map);

const data = {
    labels: [],
    datasets: [{
        backgroundColor: [
            'rgb(255, 99, 132)',
            'rgb(54, 162, 235)',
            'rgb(255, 205, 86)',
            'rgb(55, 24, 55)',
            'rgb(128, 205, 206)',
            'rgb(36, 44, 6)',
            'rgb(23, 215, 57)',
            'rgb(246, 23, 246)',
            'rgb(55, 124, 222)',
            'rgb(84, 123, 76)',
            'rgb(77, 123, 124)',
        ],
        data: [],
        hoverOffset: 4,
    }]
};

const config = {
    type: 'pie',
    data: data,
    options: {}
};

const myChart = new Chart(
    document.getElementById('myChart'),
    config
);

export const updateChart = (labels, data) => {
    myChart.data.labels = labels;
    myChart.data.datasets[0].data = data;
    myChart.update();
};

export const getDaysToBirthday = (b_day) => {
    const today = dayjs();
    let birthDate = dayjs(b_day).year(today.year());
    if (birthDate.diff(today) < 0)
        birthDate = dayjs(b_day).year(today.year() + 1);
    const daysToBirthday = birthDate.diff(today, 'day');
    return daysToBirthday;
}
import { filterUsers, sortUsers, findUser, Comparator, validateUser } from './lab3.mjs';
import { getRandUsers, getAddedUsers, postAddedUser } from './lab5.mjs';
import { map, updateChart, getDaysToBirthday } from './lab6.mjs';

let user_mock;
let gettedUsers = await getRandUsers(10);
// let addedUsers = await getAddedUsers();
let addedUsers = [];

const starSyb = "★";
const unfilledStarSyb = "☆";
const infoPopup = document.getElementById("infoPopup");
const addPopup = document.getElementById("addPopup");

let currentUser = undefined;
let currentAge = "all";
let currentGender = "all";
let currentCountry = "all";
let onlyWithPhoto = false;
let onlyFavorites = false;

let currentTablePage = 1;
let currentFavoritesPage = 1;
const pageSize = 10;
let clickNumber = 0;

const addRow = (table, user) => {
    const row = table.insertRow();
    const Name = row.insertCell();
    const Speciality = row.insertCell();
    const Age = row.insertCell();
    const Gender = row.insertCell();
    const Nationality = row.insertCell();
    Name.innerHTML = user['full_name'];
    Speciality.innerHTML = user['course'];
    Age.innerHTML = user['age'] || null;
    Gender.innerHTML = user['gender'];
    Nationality.innerHTML = user['country'];
}

const showStatistic = (users) => {
    let statTable = document.getElementById("statistic-table").getElementsByTagName("tbody")[0];
    statTable.innerHTML = "";
    users.slice((currentTablePage - 1) * pageSize, currentTablePage * pageSize).forEach((user) => addRow(statTable, user));

    let tableRefs = document.getElementsByClassName("table-ref");
    for (let ref of tableRefs) {
        if (ref.innerText === "...") continue;
        const refText = ref.innerText;
        ref.addEventListener("click", (event) => {
            statTable.innerHTML = "";

            if (refText === "Last")
                currentTablePage = Math.ceil(users.length / pageSize);
            else
                currentTablePage = Number(refText);

            users.slice((currentTablePage - 1) * pageSize, currentTablePage * pageSize).forEach((user) => addRow(statTable, user));
        });
    }
}

function showAddPopup() {
    let popup = document.getElementById("addPopup");
    popup.style.visibility = "visible";
}

function unshowAddPopup() {
    let popup = document.getElementById("addPopup");
    popup.style.visibility = "hidden";
}

function showInfoPopup() {
    let popup = document.getElementById("infoPopup");
    popup.style.visibility = "visible";
}

function unshowInfoPopup() {
    let popup = document.getElementById("infoPopup");
    popup.style.visibility = "hidden";
}

const prepareInfoPopup = (user) => {
    let popup = document.getElementById("infoPopup");
    favoriteMaker.innerText = currentUser['favorite'] ? starSyb : unfilledStarSyb;
    popup.getElementsByTagName("img")[0].src = user['picture_large'] || "./images/teacher_img.png";
    popup.getElementsByClassName("ipname")[0].innerText = user['full_name'];
    popup.getElementsByClassName("ipspeciality")[0].innerText = user['course'] || "None course";
    popup.getElementsByClassName("ipinfo")[0].innerText = `${user['age'] || "None age"} , ${user['gender']}`;
    popup.getElementsByClassName("ipemail")[0].innerText = user['email'] || "None email";
    popup.getElementsByClassName("ipphone")[0].innerText = user['phone'] || "None phone";
    popup.getElementsByClassName("ipnotes")[0].innerText = user['note'] || "None notes";
    document.getElementById('daysToB_day').innerText = 'Days to BD: ' + getDaysToBirthday(user.b_date);
    
    const coord = user['coordinates'];
    map.setView([Number(coord['latitude']) , Number(coord['longitude'])], 15);

    for (const btn of infoPopup.getElementsByClassName("exit-btn")) {
        btn.addEventListener("click", (event) => {
            unshowInfoPopup();
        });
    }

}

const favoriteFigurefromUser = (user) => {
    const figure = document.createElement("figure");
    const img = document.createElement("img");
    const name = document.createElement("figcaption");
    const sector = document.createElement("figcaption");
    const country = document.createElement("figcaption");
    const imgProxy = document.createElement("div");
    figure.classList.add("teacher-full");
    imgProxy.classList.add("img-proxy");
    img.classList.add("teacher-img");
    name.classList.add("name");
    sector.classList.add("sector");
    country.classList.add("country");

    let fname = " ";
    let lname = " ";
    const splitted = user['full_name'].split(' ');
    if (splitted.length > 1) {
        fname = splitted[0];
        lname = splitted[1];
    } else {
        fname = splitted[0];
    }
    name.innerText = `${fname}\n${lname}`;
    sector.innerText = user['course'];
    country.innerText = user['country'];
    const imgsrc = user['picture_large'];
    img.src = imgsrc;
    imgProxy.innerText = `${fname[0]}. ${lname[0]}.`;

    const isFavorite = user['favorite'];
    if (isFavorite) {
        const star = document.createElement("div");
        star.classList.add("star");
        star.innerText = starSyb;
        figure.appendChild(star);
    }
    if (typeof imgsrc !== 'undefined')
        figure.appendChild(img);
    else
        figure.appendChild(imgProxy);


    figure.appendChild(name);
    figure.appendChild(sector);
    figure.appendChild(country);

    figure.addEventListener("click", (event) => {
        currentUser = user;
        prepareInfoPopup(currentUser);
        showInfoPopup();
    });

    return figure;
};

const showTopTeachers = (users) => {
    const topTeachersContent = document.getElementById("top-teachers").getElementsByClassName("content")[0];
    const statisticCount = document.getElementById("statisticCount");
    statisticCount.innerText = users.length;
    topTeachersContent.innerHTML = "";
    for (const user of users)
        topTeachersContent.appendChild(favoriteFigurefromUser(user));
}

const showFavorites = (users) => {
    const favoritesContent = document.getElementById("favorites").getElementsByClassName("content")[0];
    favoritesContent.innerHTML = "";
    const filtred = users.filter((user) => user['favorite']);
    if ((currentFavoritesPage - 1) * 5 >= filtred.length)
        --currentFavoritesPage;
    for (const user of filtred.slice((currentFavoritesPage - 1) * 5, 5 * currentFavoritesPage))
        favoritesContent.appendChild(favoriteFigurefromUser(user));
};

const prepareTopTeachersSelector = (users) => {
    const ageSelect = document.getElementById("ageSelect");
    const genderSelect = document.getElementById("sexSelect");
    const countrySelect = document.getElementById("regionSelect");
    const photoCheckbox = document.getElementById("photoCheckbox");

    ageSelect.innerHTML = '<option value="all" selected>All</option>';
    genderSelect.innerHTML = '<option value="all" selected>All</option>';
    countrySelect.innerHTML = '<option value="all" selected>All</option>';

    photoCheckbox.checked = onlyWithPhoto;
    const favoritesCheckbox = document.getElementById("favoritesCheckbox");
    favoritesCheckbox.checked = onlyFavorites;

    const ages = new Set(users.map((user) => user['age']).filter((age) => typeof age !== 'undefined').sort((a, b) => a - b));
    const genders = new Set(users.map((user) => user['gender']).filter((gender) => typeof gender !== 'undefined'));
    const countries = new Set(users.map((user) => user['country']).filter((country) => typeof country !== 'undefined'));

    ages.forEach((age) => {
        const opt = document.createElement("option");
        opt['value'] = age;
        opt.innerText = age;
        ageSelect.appendChild(opt);
    });
    genders.forEach((gender) => {
        const opt = document.createElement("option");
        opt['value'] = gender;
        opt.innerText = gender;
        genderSelect.appendChild(opt);
    });
    countries.forEach((country) => {
        const opt = document.createElement("option");
        opt['value'] = country;
        opt.innerText = country;
        countrySelect.appendChild(opt);
    });

    const prepareFiltredUsers = (users) => {
        const value = {
        };
        const age = currentAge;
        const gender = currentGender;
        const country = currentCountry;
        if (age !== "all")
            value["age"] = Number(age);
        if (gender !== "all")
            value["gender"] = gender;
        if (country != "all")
            value["country"] = country;
        if (onlyFavorites)
            value["favorite"] = true;
        let filtred = filterUsers(users, value);
        if (onlyWithPhoto)
            filtred = filtred.filter((user) => user['picture_large'] !== undefined);
        showTopTeachers(filtred);
    };

    ageSelect.addEventListener("change", (event) => {
        currentAge = ageSelect.value;
        prepareFiltredUsers(users);
    });
    genderSelect.addEventListener("change", (event) => {
        currentGender = genderSelect.value;
        prepareFiltredUsers(users);
    });
    countrySelect.addEventListener("change", (event) => {
        currentCountry = countrySelect.value;
        prepareFiltredUsers(users);
    });
    photoCheckbox.addEventListener("change", (event) => {
        onlyWithPhoto = photoCheckbox.checked;
        prepareFiltredUsers(users);
    });
    favoritesCheckbox.addEventListener("change", (event) => {
        onlyFavorites = favoritesCheckbox.checked;
        prepareFiltredUsers(users);
    });
}

const prepareTable = (users) => {
    const table = document.getElementById("statistic-table");
    const rowNameToAttribute = {
        "Name": "full_name",
        "Speciality": "course",
        "Age": "age",
        "Gender": "gender",
        "Nationality": "country",
    };
    for (const tr of table.querySelectorAll('thead tr th')) {
        tr.addEventListener("click", (event) => {
            clickNumber %= 2;
            const comp = (clickNumber === 0) ? Comparator.greater : Comparator.less;
            clickNumber += 1;
            showStatistic(sortUsers(users, rowNameToAttribute[tr.innerText], comp));
        });
    }
};

const prepareFavoriteMaker = () => {
    const favoriteMaker = document.getElementById("favoriteMaker");
    favoriteMaker.addEventListener("click", (event) => {
        currentUser['favorite'] = !currentUser['favorite'];
        favoriteMaker.innerText = currentUser['favorite'] ? starSyb : unfilledStarSyb;
        update();
    });
}

const prepareSearch = (users) => {
    const btn = document.getElementById("search-button");
    const field = document.getElementById("search-input");

    btn.onclick = () => {
        if (field.value === "")
            return;
        const byName = findUser(users, "full_name", field.value);
        const byAge = findUser(users, "age", Number(field.value));
        const byNote = findUser(users, "note", field.value);

        if (typeof byName !== 'undefined')
            currentUser = byName;
        else if (typeof byAge !== 'undefined')
            currentUser = byAge;
        else if (typeof byNote !== 'undefined')
            currentUser = byNote;
        else {
            alert("No such user");
            return;
        }
        prepareInfoPopup(currentUser);
        showInfoPopup();
    };
}

const update = () => {
    user_mock = addedUsers.concat(gettedUsers);

    currentUser = undefined;
    currentAge = "all";
    currentGender = "all";
    currentCountry = "all";
    onlyWithPhoto = false;
    onlyFavorites = false;

    prepareTopTeachersSelector(user_mock);
    prepareAddTeacherBtns(user_mock);
    prepareSearch(user_mock);
    prepareTable(user_mock);
    showTopTeachers(user_mock);
    showFavorites(user_mock);
    showStatistic(user_mock);

    const counts = {};
    user_mock.forEach((user) => {
        counts[user.course] = (counts[user.course] || 0) + 1;
    });
    const courses = Object.keys(counts);
    const values = Object.values(counts);
    updateChart(courses, values);
}

const prepareFavoritesBtns = () => {
    const prev = document.getElementById("favoritesPrev");
    const next = document.getElementById("favoritesNext");
    prev.addEventListener("click", (event) => {
        if (currentFavoritesPage === 1)
            return;
        currentFavoritesPage -= 1;
        showFavorites(user_mock);
    });
    next.addEventListener("click", (event) => {
        if ((currentFavoritesPage) * 5 >= user_mock.length)
            return;
        currentFavoritesPage += 1;
        showFavorites(user_mock);
    });
};

const prepareAddTeacherBtns = (users) => {
    const addTeacherBtns = document.getElementsByClassName("teacher-btn");
    for (const btn of addTeacherBtns) {
        btn.addEventListener("click", (event) => {
            showAddPopup();
        });
    }
    for (const btn of addPopup.getElementsByClassName("exit-btn")) {
        btn.addEventListener("click", (event) => {
            unshowAddPopup();
        });
    }


    const countries = new Set(users.map((user) => user['country']).filter((country) => typeof country !== 'undefined'));
    const specialities = new Set(users.map((user) => user['course']).filter((course) => course !== null));
    const countriesSelect = document.getElementById("addPopupCountries");
    const specialitiesSelect = document.getElementById("addPopupSpecialities");
    countries.forEach((country) => {
        const opt = document.createElement("option");
        opt['value'] = country;
        opt.innerText = country;
        countriesSelect.appendChild(opt);
    });
    specialities.forEach((speciality) => {
        const opt = document.createElement("option");
        opt['value'] = speciality;
        opt.innerText = speciality;
        specialitiesSelect.appendChild(opt);
    });
}

const prepareNextButton = () => {
    const nextButton = document.getElementById("nextTeachersButton");
    nextButton.addEventListener("click", async (event) => {
        gettedUsers = await getRandUsers(10);
        update();
    });
}

const prepareAddPopupBtn = () => {
    document.getElementById('addPopup-btn').addEventListener('click', (event) => {
        const user = {
            "full_name": document.getElementById("addPopupName").value,
            "course": document.getElementById("addPopupSpecialities").value,
            "country": document.getElementById("addPopupCountries").value,
            "city": document.getElementById("addPopupCity").value,
            "email": document.getElementById("addPopupEmail").value,
            "phone": document.getElementById("addPopupPhone").value,
            "b_date": document.getElementById("addPopupBirthday").value,
            "gender": addPopupMale.checked ? "male" : "female",
            "bg_color": document.getElementById("addPopupColor").value,
            "note": document.getElementById("addPopupNotes").value,
            "favorite": false,
        };
        // if (validateUser(user))
        // postAddedUser(user);
        update();
        addedUsers.push(user);
        event.preventDefault();
    });
}

prepareFavoritesBtns();
prepareFavoriteMaker();
prepareAddPopupBtn();
prepareNextButton();
update();
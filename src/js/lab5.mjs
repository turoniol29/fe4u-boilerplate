import { prepareMock } from './lab3.mjs';

const randUserUrl = "https://randomuser.me/api";
const addedUsersUrl = "http://localhost:3000/users/"

const makeRequest = async (url) => {
    const response = await fetch(url);
    const data = await response.json();
    return data;
};

export const getRandUsers = async (usersCount = 1) => {
    const data = await makeRequest(randUserUrl + `?results=${usersCount}`);
    return prepareMock(data.results);
};

export const postAddedUser = async (user) => {
    const response = await fetch(addedUsersUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
    });
    return response;
}

export const getAddedUsers = async () => {
    const data = await makeRequest(addedUsersUrl);  
    return data;
}

export const rand50Users = await getRandUsers(50);
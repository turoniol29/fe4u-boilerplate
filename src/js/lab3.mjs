import { randomUserMock, additionalUsers } from './FE4U-Lab3-mock.mjs';

class Generator {
    static getID = function () {
        return Generator.#id++;
    }
    static getColor = function () {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
    static getCourse = function () {
        const courses = ['Mathematics', 'Physics', 'English', 'Computer Science', 'Dancing', 'Chess', 'Biology', 'Chemistry',
            'Law', 'Art', 'Medicine', 'Statistics'];
        return courses[Math.floor(Math.random() * courses.length)];
    }
    static getFavorite = function () {
        return Math.random() >= 0.5;
    }
    static getNote = function () {
        return 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.';
    }

    static #id = 0;
}

class Validator {
    static checkString = function (str) {
        return typeof str === 'string' && str.length > 0 && str[0].toUpperCase() == str[0];
    }
    static checkNumber = function (num) {
        return typeof num === 'number';
    }
    static checkEmail = function (email) {
        return typeof email === 'string' && email.includes('@');
    }
    static checkPhoneNumber = function (phone) {
        return typeof phone === 'string' && phone.match(/^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s./0-9]*$/g).length == 1;
    }
}

export class Comparator {
    static less = function (a, b) {
        if (a < b) return 1;
        else if (a > b) return -1;
        else return 0;
    }
    
    static greater = function (a, b) {
        if (a > b) return 1;
        else if (a < b) return -1;
        else return 0;
    }
}

export const prepareMock = function (input) {
    return input.map(function (item) {
        return {
            id: Generator.getID(),
            favorite: Generator.getFavorite(),
            course: Generator.getCourse(),
            bg_color: Generator.getColor(),
            note: Generator.getNote(),
            gender: item.gender,
            title: item.name?.title || null,
            full_name: item.name ? item.name?.first + ' ' + item.name?.last : null,
            city: item.location?.city || null,
            state: item.location?.state || null,
            country: item.location?.country || null,
            postcode: item.location?.postcode || null,
            coordinates: item.location?.coordinates || null,
            timezone: item.location?.timezone || null,
            email: item.email,
            b_date: item.dob?.date || null,
            age: item.dob?.age || null,
            phone: item.phone,
            picture_large: item.picture?.large || null,
            picture_thumbnail: item.picture?.thumbnail || null
        }
    });
}

const addAdditionalMock = function (main, additional) {
    var result = main;
    additional.forEach(function (item) {
        var finded = main.find(element => element.full_name === item.full_name);
        if (finded === undefined) {
            result.push(item);
            return;
        }

        finded.id = item.id;
        finded.favorite = item.favorite;
        finded.course = item.course;
        finded.bg_color = item.bg_color;
        finded.note = item.note;
    });
}

export const validateUser = function (item) {
    return Validator.checkString(item.full_name) &&
        Validator.checkString(item.gender) &&
        Validator.checkString(item.note) &&
        Validator.checkString(item.state) &&
        Validator.checkString(item.city) &&
        Validator.checkString(item.country) &&
        Validator.checkNumber(item.age) &&
        Validator.checkPhoneNumber(item.phone) &&
        Validator.checkEmail(item.email);
}

export const filterUsers = function (users, values) {
    return users.filter((element) => {
        const pairs = Object.entries(values);
        return pairs.every(([key, value]) => element[key] === value);
    });
}

export const sortUsers = function (users, field, comparator) {
    const res = users.slice();
    res.sort(function (a, b) {
        return comparator(a[field], b[field]);
    });
    return res;
}

export const findUser = function (users, field, value) {
    return users.find(function (element) {
        return element[field] === value;
    });
}

const countPersentage  = function (users, field, value, predicate) {
    const filtred = users.filter((element) => predicate(element[field], value));
    return filtred.length / users.length * 100;
}